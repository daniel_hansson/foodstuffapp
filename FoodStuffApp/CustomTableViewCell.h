//
//  CustomTableViewCell.h
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-02-28.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodItem.h"

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameText;
@property (weak, nonatomic) IBOutlet UILabel *energyText;
@property (weak, nonatomic) IBOutlet UILabel *fatText;
@property (weak, nonatomic) IBOutlet UILabel *carbText;
@property (nonatomic) NSString *foodNumber;

@property (nonatomic) FoodItem *foodItem;

-(void)setContentToCell;

@end
