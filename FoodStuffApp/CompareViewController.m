//
//  CompareViewController.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-15.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "CompareViewController.h"
#import "ApiDataHandler.h"

@interface CompareViewController ()
@property (nonatomic) NSArray *first;
@property (nonatomic) NSArray *second;
@property (weak, nonatomic) IBOutlet UIPickerView *favPicker;
@property (nonatomic) ApiDataHandler *dataHandler;
@property (nonatomic) NSArray *favData;
@property (nonatomic) NSMutableArray *pickerData;
@property (weak, nonatomic) IBOutlet GKLineGraph *lineGraph;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIDynamicBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;

@property (nonatomic) UILabel *foodLabel1;
@property (nonatomic) UILabel *vs;
@property (nonatomic) UILabel *foodLabel2;

@end

@implementation CompareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Sets item one data
    NSNumber *fat = [[self.itemOne.foodData objectForKey:@"nutrientValues"] objectForKey:@"fat"];
    NSNumber *carb = [[self.itemOne.foodData objectForKey:@"nutrientValues"] objectForKey:@"carbohydrates"];
    NSNumber *fiber = [[self.itemOne.foodData objectForKey:@"nutrientValues"] objectForKey:@"fibres"];
    NSNumber *protein = [[self.itemOne.foodData objectForKey:@"nutrientValues"] objectForKey:@"protein"];
    self.first = @[fat,carb,fiber,protein];
    
    self.dataHandler = [ApiDataHandler sharedApiDataHandler];
    self.favData = [self.dataHandler getFavoritesArray];
    
    self.pickerData = @[].mutableCopy;
    for (int i = 0; i < self.favData.count ; i++) {
        [self.pickerData addObject:[self.favData[i] objectForKey:@"name"]];
    }
    
    self.favPicker.dataSource = self;
    self.favPicker.delegate = self;
    
    self.lineGraph.dataSource = self;
    self.lineGraph.lineWidth = 3.0;
    
    self.foodLabel1 = [[UILabel alloc] init];
    self.vs = [[UILabel alloc] init];
    self.foodLabel2 = [[UILabel alloc] init];
    
}

//------PickerStuff
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return (int)self.pickerData.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self setDataFromIndex:row];
    [self makeAnimationStuff:row];
    [self drawDiagram];
}

- (IBAction)comparePicker:(id)sender {
}

-(void)setDataFromIndex:(NSInteger)index{
    NSDictionary *compareItem = self.favData[index];
    
    NSNumber *fat = [[compareItem objectForKey:@"nutrientValues"] objectForKey:@"fat"];
    NSNumber *carb = [[compareItem objectForKey:@"nutrientValues"] objectForKey:@"carbohydrates"];
    NSNumber *fiber = [[compareItem objectForKey:@"nutrientValues"] objectForKey:@"fibres"];
    NSNumber *protein = [[compareItem objectForKey:@"nutrientValues"] objectForKey:@"protein"];
    
    self.second = @[fat,carb,fiber,protein];
}

-(void)drawDiagram{
    [self.lineGraph reset];
    [self.lineGraph draw];
}
//------Graph stuff

- (NSInteger)numberOfLines{
    return 2;
}
- (UIColor *)colorForLineAtIndex:(NSInteger)index{
    return @[[UIColor greenColor], [UIColor redColor]][index % 2];
}
- (NSArray *)valuesForLineAtIndex:(NSInteger)index{
    return @[self.first, self.second][index % 2];
}
-(NSString *)titleForLineAtIndex:(NSInteger)index{
    return  @[@"Fett",@"Kolh",@"Fiber",@"Prote"][index];
}

-(void)makeAnimationStuff:(NSInteger)index{
    self.foodLabel1.frame = CGRectMake(30, 10, 110, 70);
    self.foodLabel2.frame = CGRectMake(self.view.frame.size.width-100, 5, 110, 70);
    self.foodLabel1.transform = CGAffineTransformMakeRotation(0.1);
    self.foodLabel2.transform = CGAffineTransformMakeRotation(0.2);
    
    [self.view addSubview:self.foodLabel1];
    self.foodLabel1.text = [self.itemOne.foodData objectForKey:@"name"];
    self.foodLabel1.numberOfLines = 3;
    [self.foodLabel1 sizeToFit];
    self.foodLabel1.textAlignment = NSTextAlignmentCenter;
    [self.foodLabel1 setFont:[UIFont boldSystemFontOfSize:12]];
    self.foodLabel1.textColor = [UIColor greenColor];
    
    self.vs.frame = CGRectMake(self.view.center.x -35, 10, 70, 40);
    [self.view addSubview:self.vs];
    self.vs.text = @"VS";
    self.vs.textAlignment = NSTextAlignmentCenter;
    [self.vs setFont:[UIFont boldSystemFontOfSize:34]];
    self.vs.textColor = [UIColor blackColor];
    
    [self.view addSubview:self.foodLabel2];
    self.foodLabel2.text = [self.favData[index] objectForKey:@"name"];
    self.foodLabel2.numberOfLines = 3;
    [self.foodLabel2 sizeToFit];
    self.foodLabel2.textAlignment = NSTextAlignmentCenter;
    [self.foodLabel2 setFont:[UIFont boldSystemFontOfSize:12]];
    self.foodLabel2.textColor = [UIColor redColor];
    
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.foodLabel1, self.foodLabel2, self.vs]];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.foodLabel1, self.foodLabel2, self.vs]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    CGPoint corner = CGPointMake(self.lineGraph.frame.origin.x +
                                 self.lineGraph.frame.size.width, self.lineGraph.frame.origin.y);
    [self.collision addBoundaryWithIdentifier:@"graph" fromPoint:self.lineGraph.frame.origin toPoint:corner];
    
    [self.animator addBehavior:self.gravity];
    [self.animator addBehavior:self.collision];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
}

@end
