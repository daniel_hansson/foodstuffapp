//
//  CompareViewController.h
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-15.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodItem.h"
#import <GKLineGraph.h>

@interface CompareViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, GKLineGraphDataSource>

@property (nonatomic) FoodItem *itemOne;
@property (nonatomic) FoodItem *itemTwo;

@end
