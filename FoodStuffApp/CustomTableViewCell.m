//
//  CustomTableViewCell.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-02-28.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "CustomTableViewCell.h"



@implementation CustomTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setContentToCell{
    self.carbText.text = [NSString stringWithFormat:@"Kolhydrat: %@", [[self.foodItem.foodData objectForKey:@"nutrientValues"] objectForKey:@"carbohydrates"]];
    self.fatText.text = [NSString stringWithFormat:@"Fett: %@", [[self.foodItem.foodData objectForKey:@"nutrientValues"] objectForKey:@"fat"]];
    self.energyText.text = [NSString stringWithFormat:@"Energi: %@", [[self.foodItem.foodData objectForKey:@"nutrientValues"] objectForKey:@"energyKcal"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
