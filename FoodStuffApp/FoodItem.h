//
//  FoodItem.h
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-03.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodItem : NSObject

@property (nonatomic) NSDictionary *foodData;

-(instancetype)initWithFoodData:(NSDictionary*)data;

@end
