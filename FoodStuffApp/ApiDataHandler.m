//
//  ApiDataHandler.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-01.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "ApiDataHandler.h"
#import "FoodItem.h"
@interface ApiDataHandler()
@property (nonatomic) NSUserDefaults *savedData;
@property (nonatomic) NSMutableArray *favorites;
@end

@implementation ApiDataHandler

+(ApiDataHandler*)sharedApiDataHandler{
    static ApiDataHandler *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        
        self.favorites = [[NSMutableArray alloc]init].mutableCopy;
        self.savedData = [NSUserDefaults standardUserDefaults];
        
        [self loadFavorites];
    }
    return self;
}

-(void)getFoodItemsFromSearchFrase:(NSString*)searchFrase andSetToTableView:(FoodTableViewController*)destination{
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", searchFrase];
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error: %@",error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSArray *result =  [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if (jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            destination.foodArray = result;
            [destination.tableView reloadData];
        });
    }];
    [task resume];
}

-(void)getFoodItemsFromFavoritesAndSetToTableView:(FoodTableViewController*)destination{
    [self loadFavorites];
    destination.foodArray = self.favorites;
}

-(void)createFoodItemFromNumber:(NSString*)number andSetToCell:(CustomTableViewCell*)cell{
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", number];
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Error: %@",error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSDictionary *result =  [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        if (jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.foodItem = [[FoodItem alloc] initWithFoodData:result];
            [cell setContentToCell];
        });
    }];
    [task resume];
    
}

-(void)saveFoodItemToFavorites:(FoodItem*)item{
    [self.favorites addObject:item.foodData];
    [self saveFavorites];
}

-(void)removeFoodItemFromFavorites:(FoodItem*)item{
    int index = -1;
    for (int i = 0; i < self.favorites.count ; i++) {
        if (item.foodData[@"number"] == self.favorites[i][@"number"]) {
            index = i;
        }
    }
    if (index>-1) {
        [self.favorites removeObjectAtIndex:index];
        [self saveFavorites];
    }
}

-(void)loadFavorites{
    NSArray *temp = [self.savedData objectForKey:@"favoritesSavedData"];
    if (temp) {
        self.favorites = temp.mutableCopy;
    }
}

-(void)saveFavorites{
    NSUserDefaults *savedData = [NSUserDefaults standardUserDefaults];
    [savedData setObject:self.favorites forKey:@"favoritesSavedData"];
    [savedData synchronize];
}

-(BOOL)isItemInFavorites:(FoodItem*)item{
    for (int i = 0; i < self.favorites.count ; i++) {
        if ([item.foodData isEqualToDictionary:self.favorites[i]]) {
            return YES;
        }
    }
    return NO;
}

-(NSArray*)getFavoritesArray{
    return self.favorites;
}

@end














