//
//  DetailViewController.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-04.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "DetailViewController.h"
#import "ApiDataHandler.h"
#import "CompareViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *energy;
@property (weak, nonatomic) IBOutlet UILabel *fat;
@property (weak, nonatomic) IBOutlet UILabel *carbs;
@property (weak, nonatomic) IBOutlet UILabel *fiber;
@property (weak, nonatomic) IBOutlet UILabel *protein;
@property (weak, nonatomic) IBOutlet UILabel *salt;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (nonatomic) ApiDataHandler *dataHandler;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *healthPointLabel;
@property (weak, nonatomic) IBOutlet UIButton *compareButton;


@property (weak, nonatomic) IBOutlet UIImageView *foodImage;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataHandler = [ApiDataHandler sharedApiDataHandler];
    
    if ([self.dataHandler isItemInFavorites:self.item]) {
        self.saveButton.hidden = YES;
        self.removeButton.hidden = NO;
    }else {
        self.saveButton.hidden = NO;
        self.removeButton.hidden = YES;
    }
    
    if ([self.dataHandler getFavoritesArray].count==0) {
        [self.compareButton setTitle:@"Inga varor i Favoriter att jämnföra med" forState:normal];
        [self.compareButton setEnabled:NO];
    }
    
    self.name.text = [NSString stringWithFormat:@"%@",[self.item.foodData objectForKey:@"name"]];
    
    self.energy.text = [NSString stringWithFormat:@"Energi: %@kcal",[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"energyKcal"]];
    
    self.fat.text = [NSString stringWithFormat:@"Fett: %@g",[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"fat"]];
    
    self.carbs.text = [NSString stringWithFormat:@"Kolhydrat: %@g",[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"carbohydrates"]];
    
    self.fiber.text = [NSString stringWithFormat:@"Fiber: %@g",[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"fibres"]];
    
    self.protein.text = [NSString stringWithFormat:@"Protein: %@g",[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"protein"]];
    
    self.salt.text = [NSString stringWithFormat:@"Salt: %@g",[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"salt"]];
    
    // Health stuff
    float fatRDI = [[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"fat"] floatValue]/80;
    float carbsRDI = [[[self.item.foodData objectForKey:@"nutrientValues"] objectForKey:@"carbohydrates"] floatValue]/275;
    float totRDI = fatRDI + carbsRDI;
    int healthPoint = 10 - (totRDI*10);
    if (healthPoint<0) {
        healthPoint = 0;
    }
    self.healthPointLabel.text = [NSString stringWithFormat:@"Nyttighetsvärde: %d/10",healthPoint];
    
    
    // Image stuff
    UIImage *cachedImage = [UIImage imageWithContentsOfFile:[self imagePath]];
    if(cachedImage) {
        self.foodImage.image = cachedImage;
    } else {
        NSLog(@"No image found.");
    }
}

- (IBAction)removeFromFavorites:(id)sender {
    self.removeButton.enabled = NO;
    [self.dataHandler removeFoodItemFromFavorites:self.item];
}
- (IBAction)saveToFavorites:(id)sender {
    self.saveButton.enabled = NO;
    [self.dataHandler saveFoodItemToFavorites:self.item];
}

- (IBAction)takePhoto:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum ]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    }
}

- (NSString*)imagePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = paths[0];
    NSString *imagename = [NSString stringWithFormat:@"food%@.png",[self.item.foodData objectForKey:@"number"]];
    return [path stringByAppendingPathComponent:imagename];
}

- (void)imagePickerController:(UIImagePickerController *)imagePicker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {

    [imagePicker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = info[UIImagePickerControllerEditedImage];
    
    self.foodImage.image = image;
    
    NSData *imageData = UIImagePNGRepresentation(image);
    BOOL success = [imageData writeToFile:[self imagePath] atomically:YES];
    if(success) {
        NSLog(@"Saved FoodImage");
    } else {
        NSLog(@"Failed to save FoodImage.");
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)imagePicker {
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    CompareViewController *destination = [segue destinationViewController];
    destination.itemOne = self.item;
}

@end
