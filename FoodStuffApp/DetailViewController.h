//
//  DetailViewController.h
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-04.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodItem.h"

@interface DetailViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic) FoodItem *item;

@end
