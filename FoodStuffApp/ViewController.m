//
//  ViewController.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-02-27.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "ViewController.h"
#import "ApiDataHandler.h"
#import "FoodTableViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (strong, nonatomic) ApiDataHandler *dataHandler;
@property (nonatomic) CGRect *imageRect;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.dataHandler = [ApiDataHandler sharedApiDataHandler];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    UIImageView *logo =[[UIImageView alloc] initWithFrame:CGRectMake(1, 1, self.view.frame.size.width-12, self.view.frame.size.width-12)];
    logo.center = self.view.center;
    
    logo.contentMode = UIViewContentModeScaleAspectFit;
    logo.image=[UIImage imageNamed:@"Burger.png"];
    [self.view addSubview:logo];

    [self doAmazingAnimationWith:logo];
    
}

-(void)doAmazingAnimationWith:(UIImageView*)view{
    CGAffineTransform big = CGAffineTransformMakeScale(0.5, 0.5);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    view.transform = big;
    view.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.width/2);
    [UIView commitAnimations];
    
}

-(void)dismissKeyboard {
    [self.searchField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [self.view endEditing:YES];
    
    if ([segue.identifier isEqualToString:@"showSearchResult"]) {
        FoodTableViewController *destination = [segue destinationViewController];
        [self.dataHandler getFoodItemsFromSearchFrase:self.searchField.text andSetToTableView:destination];
        self.searchField.text = @"";
    }else if([segue.identifier isEqualToString:@"showFavorites"]){
        FoodTableViewController *destination = [segue destinationViewController];
        [self.dataHandler getFoodItemsFromFavoritesAndSetToTableView:destination];
    }
    
}

@end
