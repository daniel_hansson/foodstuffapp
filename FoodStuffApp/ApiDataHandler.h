//
//  ApiDataHandler.h
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-01.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FoodTableViewController.h"
#import "CustomTableViewCell.h"

@interface ApiDataHandler : NSObject

+(ApiDataHandler*)sharedApiDataHandler;

-(void)getFoodItemsFromSearchFrase:(NSString*)searchFrase andSetToTableView:(FoodTableViewController*)destination;

-(void)getFoodItemsFromFavoritesAndSetToTableView:(FoodTableViewController*)destination;

-(void)createFoodItemFromNumber:(NSString*)number andSetToCell:(CustomTableViewCell*)cell;

-(void)saveFoodItemToFavorites:(FoodItem*)item;

-(void)removeFoodItemFromFavorites:(FoodItem*)item;

-(BOOL)isItemInFavorites:(FoodItem*)name;

-(NSArray*)getFavoritesArray;

@end
