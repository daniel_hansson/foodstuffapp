//
//  FoodTableViewController.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-02-27.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "FoodTableViewController.h"
#import "CustomTableViewCell.h"
#import "DetailViewController.h"
#import "ApiDataHandler.h"

@interface FoodTableViewController ()

@property (nonatomic) ApiDataHandler *dataHandler;

@end

@implementation FoodTableViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataHandler = [ApiDataHandler sharedApiDataHandler];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.foodArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    [self.dataHandler createFoodItemFromNumber:[self.foodArray[indexPath.row] objectForKey:@"number"] andSetToCell:cell];
    
    cell.nameText.text = [self.foodArray[indexPath.row] objectForKey:@"name"];

    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(CustomTableViewCell*)sender {
    
    DetailViewController *destination = [segue destinationViewController];
    destination.item = sender.foodItem;
}


@end
