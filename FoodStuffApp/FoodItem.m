//
//  FoodItem.m
//  FoodStuffApp
//
//  Created by Daniel Hansson on 2016-03-03.
//  Copyright © 2016 Daniel Hansson. All rights reserved.
//

#import "FoodItem.h"

@implementation FoodItem

-(instancetype)initWithFoodData:(NSDictionary*)data{
    self = [super init];
    if (self) {
        self.foodData = data;
    }
    return self;
}



@end
